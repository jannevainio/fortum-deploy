# fortum-deploy config v 1.1

This is git submodule for fortum-staging and fortum-support repositories, use only through them.

Do not edit fortum-deploy dir from those projects directly.

What does this module contain:

For external "API", it contains grunt task fortumDeploy.
You can deploy the fortum-staging or fortum-support using this task by doing the following:


1. Get the latest version from git, which you want to deploy, the deployment process deploys the checked out
version

2. Enter the project directory (fortum-staging or fortum-support)

3. Check the deploy.json in the project directory, example:
appRootName is either app-tool or app, you should not have need to edit this, if deployment process stays the same.
SO ONLY EDIT deployRev (packagename) AND version BEFORE DOING NEW DEPLOY
{
  "appRootName": "app-tool",
  "deployRev": "supportTool_sprint2016-4-1.0.4",
  "version": "1.0.4"
}

4. Deploy the code using grunt deployCODE, deploySTAGING or deployPRODUCTION


The content of this deployment submodule:
-----------------------------------------

id_rsa : ssh-key to access deployment servers in therecorp

deploy.js: Grunt file to contain deployment tasks, this file is included in Gruntfile.js in fortum-staging
and fortum-support, generally you may have to edit this file only if something in deployment process is significantly
changing, usually it is enough to just edit json-file

deployCommons.json: The settings of this deployment module are contained here. Example of this file is show below
(real settings on 7.3.2016):

distDir is the source dir to contain build  code in project dir, containing all the stuff to be deployed

mgmt list all info necessary to enter the management-server, which act as a code distribution hub

code, staging and production contains info necessary to enter code, staging and deployment servers
from mgmt-server using ssh

nodeTargets contains list of nodes, where the actual code is distributed from mgmt-server
You can easily add more nodes to this list, they are automatically taken into use in deployment process.
targetDir contains the place to copy the distribution package, appRoot is the actual web root, in which a symbolic link
is created to point the distribution package in targetDir

{
  "distDir": "dist/",

  "mgmt": {
    "host" : "mgmt.theregate.net",
    "username" : "leadin",
    "key": "id_rsa",
    "deployRoot": "/home/leadin/code_deploy/"
  },

  "code": {
    "server": "code",
    "user": "fortum"
  },
  "staging": {
    "server": "there",
    "user": "fortum"

  },
  "production": {
    "server": "fiksu",
    "user": "fortum"

  },
  "nodeTargets": {
    "web1": {
      "targetDir": "/home/fortum/deployments/",
      "appRoot": "/home/fortum/app-root/"

    },
    "web2": {
      "targetDir": "/home/fortum/deployments/",
      "appRoot": "/home/fortum/app-root/"
    }
  }
}
