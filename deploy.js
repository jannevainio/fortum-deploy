/**
 * Created by jannevainio on 05/03/16.
 */
module.exports = function(grunt) {

  grunt.loadNpmTasks('grunt-ssh');


  grunt.config('commonDeployConfig', grunt.file.readJSON('./fortum-deploy/deployCommons.json'));
  grunt.config('id_rsa', grunt.file.read(process.env['HOME'] + '/.ssh/id_rsa'));

  grunt.config('sftp', {
    mgmt: {
      files: {
        "./": '<%= commonDeployConfig.distDir %>' + "**"
      },
      options: '<%= sftpOptions %>'
    }
  });

  var sshexecCommands = function(server, node, targetDir, user, appRoot, app) {
    return [
      'scp -r ' + '<%= commonDeployConfig.mgmt.deployRoot %>' + '<%= fortumDeploy.deployRev %>' + ' ' + user + '@' + server + '-' + node + ':' + targetDir,
      'ssh ' + user + '@' + server + '-' + node + ' \'rm ' + appRoot + app + '; ln -s ' + targetDir + '<%= fortumDeploy.deployRev %> ' + appRoot + app + '\''

    ];
  };

  grunt.registerMultiTask('multiWebDeploy', 'Distribute code to servers web1, web2,...', function(parms) {
    var self = this;
    grunt.log.writeln("Distributing code to " + self.target + " " + self.data.deploy);

    grunt.config('sshexec', {
      code: {
        command: sshexecCommands('<%= commonDeployConfig.code.server %>', self.target,
          self.data.targetDir, '<%= commonDeployConfig.code.user %>', self.data.appRoot, self.data.app),
        options: grunt.config('sshexecOptions')
      },
      staging: {
        command: sshexecCommands('<%= commonDeployConfig.staging.server %>', self.target,
          self.data.targetDir, '<%= commonDeployConfig.staging.user %>', self.data.appRoot, self.data.app),
        options: grunt.config('sshexecOptions')
      },
      production: {
        command: sshexecCommands('<%= commonDeployConfig.production.server %>', self.target,
          self.data.targetDir, '<%= commonDeployConfig.production.user %>', self.data.appRoot, self.data.app),
        options: grunt.config('sshexecOptions')
      }
    });

    grunt.task.run('sshexec:' + self.data.deploy);

  });

  grunt.registerTask('copyToMgmtServer', 'Copy code to mgmt server', function() {

    var self = this;

    grunt.task.run('sftp:mgmt');

  });

  var getmgmt = function(parms) {
    if (parms === 'code') {
      return grunt.config('commonDeployConfig').codemgmt;
    } else {
      return grunt.config('commonDeployConfig').mgmt;
    }
  };

  var getShhexecOptions = function() {
    return {
      host: grunt.config('mgmt').host,
      username: grunt.config('mgmt').username,
      privateKey: grunt.config('id_rsa')
    };
  };

  var getSftpOptions = function() {
    return {
      path: grunt.config('mgmt').deployRoot + '<%= fortumDeploy.deployRev %>',
      host: grunt.config('mgmt').host,
      username: grunt.config('mgmt').username,
      privateKey: grunt.config('id_rsa'),
      showProgress: true,
      createDirectories: true,
      srcBasePath: '<%= commonDeployConfig.distDir %>'
    };
  };


  grunt.registerTask('fortumDeploy', 'Deployments for code, staging and production', function(parms) {

    var self = this;

    if (!parms)
      return;
    /* Only copy in code deploy, so you can not deploy untested code to staging or production */
    grunt.config('mgmt', getmgmt(parms));
    grunt.config('sshexecOptions', getShhexecOptions());
    grunt.config('sftpOptions', getSftpOptions());

    grunt.task.run('copyToMgmtServer');

    depOpts = grunt.config('commonDeployConfig.nodeTargets');
    for (var key in depOpts) {
      if (depOpts.hasOwnProperty(key)) {
        depOpts[key].deploy = parms;
        depOpts[key].app = '<%= fortumDeploy.app %>';
      }
    }
    depOpts = grunt.config('commonDeployConfig.nodeTargets', depOpts);
    grunt.config('multiWebDeploy', grunt.config('commonDeployConfig.nodeTargets'));

    grunt.task.run('multiWebDeploy');
  });

};
